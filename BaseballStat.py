import re
import operator

testfile = open('cardinals-1940.txt', 'r')
player_info = {}
player_result = {}

for line in testfile:
    if re.match(r'(.*) batted ([0-9]+) times with ([0-9]+) hits and ([0-9]+) runs', line):
        search_data = re.findall(r'(.*) batted ([0-9]+) times with ([0-9]+) hits and ([0-9]+) runs', line)

        player = search_data[0][0]
        batted = int(search_data[0][1])
        hits = int(search_data[0][2])
        runs = int(search_data[0][3])

        if player in player_info:
            player_info[player]['batted'] += batted
            player_info[player]['hits'] += hits
            player_info[player]['runs'] += runs
        else:
            player_info[player] = {}
            player_info[player]['batted'] = batted
            player_info[player]['hits'] = hits
            player_info[player]['runs'] = runs

    else:
         print('This line has incorrect data! (eg. XXX batted # times with # hits and # runs) (original. %s)' % (line.replace('\n', '')))

for player in player_info.keys():
    batting_average = float(player_info[player]['hits'] / player_info[player]['batted'])
    player_result[player] = round(batting_average, 3)

sort_player_result = sorted(player_result.items(), key=operator.itemgetter(1), reverse=True)

for status in sort_player_result:
    print('%s: %.3f' % (status[0], status[1]))
